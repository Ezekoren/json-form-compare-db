var obj;

document.addEventListener('DOMContentLoaded', function() { //Funcion que se ejecuta al cargar la pagina
    requestJson();
}, false);


function requestJson() {
    var xml = new XMLHttpRequest(); //Declaramos un nuevo request XML
    xml.overrideMimeType("application/json"); //Declaramos que el tipo de request sera del tipo application/json
    xml.open("GET", "./data.json", true); //Abrimos el archivo "./data.json"
    xml.onreadystatechange = function() { //Funcion que se ejecuta cuando se recibe informacion
        if (xml.readyState === 4 && xml.status == "200") { //Revisamos que la informacion se recibio correctamente
            obj = JSON.parse(xml.responseText); //Convertimos la string de json recibida en un objeto
            console.log(obj);
        }
    }
    xml.send(null); //Accedemos al archivo, sin mandarle nada, para poder leer el contenido
}

function compareForm() {
    var user = document.forms["login"]["user"].value;
    var pass = document.forms["login"]["pass"].value;

    var correct = false;
    var users = obj.users;
    for (var key in users) {
        console.log(users[key]);
        var juser = users[key]["user"];
        var jpass = users[key]["pass"];
        if (juser == user && jpass == pass) {
            correct = true;
        };
    };
    if (correct == true) {
        alert("Success");
    } else {
        alert("failed");
    };
};